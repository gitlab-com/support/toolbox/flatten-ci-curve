#!/opt/gitlab/embedded/bin/ruby
# frozen_string_literal: true

require 'csv'
require 'gitlab'
require 'open3'
require 'optimist'

opts = Optimist.options do
  opt :fqdn, 'GitLab external URL', type: :string
  opt :apat, 'Personal Access Token with api scope from an admin user', type: :string
  opt :hour, 'Randomize the hour, in addition to the minute value.', type: :boolean
end

# Processes CI pipeline schedules, after extracting them from the DB.
class FlattenCiCurve
  attr_reader :glc, :issue_title, :issue_body, :schedules_csv

  def initialize(fqdn, apat)
    @endp = "https://#{fqdn}/api/v4"
    @glc = Gitlab.client(endpoint: @endp, private_token: apat || ENV['GITLAB_API_PRIVATE_TOKEN'])

    # Input data from fast DB instead of slower API
    @cron_query = 'SELECT id, project_id, cron, cron_timezone, description, owner_id FROM ci_pipeline_schedules;'
    @psql_cmmd = "sudo gitlab-psql --csv --command='#{@cron_query}'"
    @schedules_csv = query_schedules

    # Communication to projects via API
    @template = File.readlines('issue-template.md')
    @issue_title = @template.first
    @issue_body = @template - [@issue_title]
  end

  def iterate_over_projects
    schedules_csv.each do |row|
      # We ignore the CSV's header line,
      # as well as schedules which already define a non-peak minute.
      next if row['project_id'].eql? 'project_id'
      next if row['cron'].match?(/^[1-9]/)

      proj_path = glc.project(row['project_id']).web_url
      post_issue(proj_path, row)
      sleep 1
    end
  end

  private

  def post_issue(proj_path, row)
    glc.create_issue(
      row['project_id'],
      fill_title(row),
      {
        description: fill_template(row, proj_path),
        assignee_id: row['owner_id']
      }
    )
  end

  def shuffle_cron(row)
    schedule = row['cron'].split(' ')
    minute = schedule[0]
    hour = schedule[1]

    # Deterministically seed the later #rand(..)
    srand(row['project_id'].to_i)

    if opts[:hour]
      rest = schedule[2, 4].join(' ')
      "#{minute} #{rand(0..23)} #{rest}"
    else
      rest = schedule[1, 4].join(' ')
      "#{rand(1..59)} #{rest}"
    end
  end

  def query_schedules
    csv, err, _exit = Open3.capture3(@psql_cmmd)
    puts err if err

    CSV.parse(csv, headers: true)
  end

  def truncate(string, limit)
    return string if string.length < limit

    "#{string[0, limit]}…"
  end

  def fill_title(row)
    issue_title.sub('SCHED_ID', truncate(row['description'], 20)).chomp
  end

  def fill_template(row, proj_path)
    line = []
    line << "[#{truncate(row['description'], 50)}]"
    line << "(#{proj_path}/-/pipeline_schedules/#{row['id']}/edit): "
    line << "Change to `#{shuffle_cron(row)}`"

    issue_body.join('').sub('SCHED_LINE', line.join(''))
  end
end

FlattenCiCurve.new(opts[:fqdn], opts[:apat]).iterate_over_projects
