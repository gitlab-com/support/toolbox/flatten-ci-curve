Optimize schedule of CI pipeline: SCHED_ID
Hello!

This is to notify you of a suggestion by your GitLab admin to
slightly optimize your CI pipeline schedule:

```
SCHED_LINE
```

If you have multiple schedules, one issue each has been created.
In that case, please adjust this `cron` suggestion
by +/- a few minutes for each schedule,
to further distribute the resulting load.

---

This is an instance-wide effort to avoid
too many CI jobs starting in a too narrow timeframe.
In such situations, [GitLab's tested `requests per second (RPS)` rates](https://docs.gitlab.com/ee/administration/reference_architectures/)
can unintentionally be exceeded.

Please help keep our instance load manageable,
and thus performance acceptable.

Thank you & kind regards!
