# Flatten _ci_ curve!

> Spoken with a French accent

As shown in [gitlab#30038](https://gitlab.com/gitlab-org/gitlab/-/issues/30038#relevant-logs-andor-screenshots),
instances can accumulate quite a number of projects that (for whatever reasons)
use the same CI schedule.

This script posts an issue for each pipeline schedule
with a suggestion to change the schedule a bit.
Essentially, this is a workaround while 
[gitlab#17799 (… Cron project hash …)](https://gitlab.com/gitlab-org/gitlab/-/issues/17799)
isn't yet implemented.

Similar to unoptimized [API clients/scripts](https://docs.gitlab.com/ee/administration/logs/log_parsing.html#print-top-api-user-agents),
running at high frequency,
too many CI jobs getting scheduled at the same time,
can cause performance problems on Rails and or Gitaly.

## Installation

1. On your GitLab server: `git clone` this repo and `cd` into it.
2. Run `/opt/gitlab/embedded/bin/bundle install`

## Usage

First, adjust the text of [issue-template.md](./issue-template.md) as needed,
but don't change the `SCHED_*` placeholders.

```shell
# either
export GITLAB_API_PRIVATE_TOKEN=<by-admin-user-with-API-scope>
./flatten_ci_curve --fqdn=mygitlab.xyz

# or
./flatten_ci_curve --fqdn=mygitlab.xyz --apat=<by-admin-user-with-API-scope>
```

Optionally, use the `--hour` flag to also randomize the suggested hour value.

## Caveats

1. On scaled/HA/multi-node Omnibuses, this script should work on any node with DB access.
   If `which gitlab-psql` yields a path, this script should work as well.
1. Inactive schedules are included, because they could be activated at any time. Better to also adjust those while we're at it.

## Further reading

Aside using from this script to flatten the curves/spikes, so to speak, review:

- https://docs.gitlab.com/ee/ci/pipelines/pipeline_efficiency.html
- https://docs.gitlab.com/ee/ci/large_repositories/

## Contributing

MRs are welcome! For major changes, please first open an issue to discuss your suggestion.

## License: [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html)

Copyright (c) 2023 Katrin Leinweber (GitLab Inc.)
